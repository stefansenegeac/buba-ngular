/**
 * Created by Andrei on 16.01.2016.
 */
(function (){

    'use strict';

    var bubaApp = angular.module('bubaApp', [
        'ngResource',
        //'ngRoute',
        'ui.router',
        'ui.grid',
        'ngCookies'
    ]);

    function bubaAppConfig ($stateProvider, $urlRouterProvider){
        $stateProvider
            .state('home', {
                url: "/home",
                templateUrl: "public/views/index.html",
                controller:'MainCtrl'
            })
            .state('user-grid', {
                url: "/user-grid",
                templateUrl: "public/views/user-grid.html",
                controller: 'GridCtrl'
            })
            .state('user-profile',{
                url: "/user-profile",
                templateUrl: "public/views/user-profile.html",
                controller: 'MainCtrl'
            })
            .state('add-user', {
                url: "/add-user",
                templateUrl: "public/views/add-user.html",
                controller: 'MainCtrl'
            })
            .state('user-login', {
                url: "/user-login",
                templateUrl: "public/views/user-login.html",
                controller: 'LoginCtrl'
            })
            .state('user-logout', {
                url: "/user-logout",
                templateUrl: "public/views/user-login.html",
                controller: 'LoginCtrl'
            });

        $urlRouterProvider.otherwise("user-login");
    }

    bubaAppConfig
        .$inject = ['$stateProvider', '$urlRouterProvider'];

    angular.module('bubaApp')
        .config(bubaAppConfig);

    window.bubaApp = bubaApp;

}());