/**
 * Created by stefanwork on 16-Jan-16.
 */
(function() {
    'use strict';

    function GridCtrl($scope, GridService, $cookies) {
        var self = this;

        $scope.gridData = {};

        $scope.user = $cookies.getObject('user');
        if($scope.user.role === 'master') {
            $scope.gridData = GridService.makeData();
            console.log($scope.gridData);
        }


        return ($scope.GridCtrl = self);

    }
    GridCtrl.$inject = ['$scope','GridService','$cookies'];

    bubaApp
        .controller('GridCtrl', GridCtrl);

}());
