(function(){

    'use strict';

    function LoginCtrl($scope, $state, $cookies, LoginService) {

        var self = this;
        $scope.formData = {};

        $scope.loginProcess = function(username, password){
            console.log($scope.formData);
            $scope.makelogin = LoginService.makelogin({}, $scope.formData).$promise.then(function(login){

                if(login.success == true) {
                    $state.go('home');
                    $scope.errlogin = false;
                    $cookies.putObject('user', login.user);
                }else{
                    $scope.errlogin = true;
                }

            });

        }
        return ($scope.LoginCtrl = self);
    }

    LoginCtrl.$inject = ['$scope', '$state', '$cookies', 'LoginService'];


    angular.module('bubaApp')
        .controller('LoginCtrl', LoginCtrl);

}());