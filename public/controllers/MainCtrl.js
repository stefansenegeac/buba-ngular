/**
 * Created by Andrei on 16.01.2016.
 */
(function () {

    'use strict';

    function MainCtrl($scope, $cookies, $state, LoginService) {

        var self = this;

        console.log($cookies.getObject('user'));
        if (!$cookies.getObject('user')) {
            $state.go('user-login');
        }
        return ($scope.MainCtrl = $scope.self);
    }

    MainCtrl.$inject =['$scope', '$rootScope', '$cookies'];

    angular.module('bubaApp')
        .controller('MainCtrl', MainCtrl);

}());