/**
 * Created by Andrei on 16.01.2016.
 */
(function () {

    'use strict';

    function navBar () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'public/views/components/navbar.html',
            controller: 'LoginCtrl'
        }
    }

    bubaApp.directive('navBar', navBar);

}());