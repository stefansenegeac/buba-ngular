
(function(){

    'use strict';

    function GridService($resource){

        return $resource ('http://192.168.150.235:6969/api/user/admin/all', {}, {
            makeData: {
                method: 'GET',
                isArray: true
            }
        });
    }

    GridService.$inject = ['$resource']

    angular.module('bubaApp')
        .factory('GridService', GridService);
}());