/**
 * Created by sebas on 1/16/2016.
 */

bubaApp.factory('sessionInjector', ['SessionService', function(SessionService) {
    var sessionInjector = {
        request: function(config) {
            if (!SessionService.isAnonymus) {
                config.headers['Authorization'] = SessionService.token;
            }
            return config;
        }
    };
    return sessionInjector;
}]);
bubaApp.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('sessionInjector');
}]);