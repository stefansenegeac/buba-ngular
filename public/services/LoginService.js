/**
<<<<<<< HEAD
 * Created by sebas on 1/16/2016.
 */
//
(function(){

    'use strict';
    var currentUser = null;

    function LoginService($resource, $state, $cookies){

        if($cookies.getObject('user')) {
            $state.go('home');
        }
        return $resource ('http://192.168.150.235:6969/auth/login', {}, {
            makelogin: {
                method: 'POST',
                isArray: false
            }
        });
    }

    LoginService.$inject = ['$resource', '$state', '$cookies']


    angular.module('bubaApp')
        .factory('LoginService', LoginService);
}());